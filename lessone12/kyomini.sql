-- phpMyAdmin SQL Dump
-- version phpStudy 2014
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2015 年 01 月 30 日 14:25
-- 服务器版本: 5.5.38
-- PHP 版本: 5.3.29

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `kyomini`
--

-- --------------------------------------------------------

--
-- 表的结构 `article`
--

CREATE TABLE IF NOT EXISTS `article` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `author` varchar(255) NOT NULL,
  `time` date NOT NULL,
  `hot` int(11) NOT NULL,
  `summary` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `article`
--

INSERT INTO `article` (`id`, `title`, `content`, `author`, `time`, `hot`, `summary`) VALUES
(1, '中国今年为何要大阅兵？', '中国将在反法西斯战争胜利70周年纪念时举行阅兵式。对这一传言，当时有些半信半疑。因为，我国从1949到1959年是每年国庆都有阅兵。1960年9月，中共中央、国务院本着厉行节约、勤俭建国的方针，决定改革国庆典礼制度，实行“五年一小庆、十年一大庆，逢大庆举行阅兵。”1964年国防部颁布的军队列条令中，首次出现阅兵条款。之后，由于“文化大革命”的缘故及其他方面的原因，中国连续24年没有举行国庆阅兵。直到1981年，根据邓小平的提议，中共中央、中央军委决定恢复阅兵，并于1984年国庆35周年时，举行了恢复阅兵后第一次大型的国庆阅兵。再接下来就重新回到了逢10年一次阅兵的规律，1999年建国50周年阅兵一次，2009年建国60周年阅兵一次。如果按照10年一大庆的规律，那么下次阅兵应该是在2019年举行。', '新浪网', '2015-01-27', 122, '过去数年，我国军事装备有了巨大提升，一大批新装备入役，一些诸如J20、J31、Y20等新战机正在试飞，更有一批新导弹在研制或入役...201');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
