<?php
    define('KYOMINI_GO',true);	
    require_once('admin_conn.php');
	require_once ('../Config/conn.php"');
    session_start();
	
	$page=isset($_GET['page'])?intval($_GET['page']):1;       
    $num=5;      //设定每页为五页  
    $total=$db->count("article"); 
    $pagenum=ceil($total/$num);    

	if($page>$pagenum || $page == 0){
	    echo '<style type="text/css">*{ padding: 0; margin: 0; } div{ padding: 4px 48px;} body{ background: #fff; font-family: "微软雅黑"; color: #333;font-size:24px} h1{ font-size: 100px; font-weight: normal; margin-bottom: 12px; } p{ line-height: 1.8em; font-size: 36px } a,a:hover,{color:blue;}</style><div style="padding: 24px 48px;"> <h1>:(</h1><p>很抱歉，此页没有数据！';
	   echo '<script>setTimeout(\'location="Article_list.php"\', 50);</script>';
	   exit;
	}

	$offset=($page-1)*$num;       
	$info=$db->select("article", array(
		"title",
		"id",),
		 array(
		 "ORDER" => "id DESC",
		 "LIMIT" => array($offset,$num)
	));
?>

<!--嵌入页头文件-->
<?php  include_once('Admin_header.php'); ?>



<body>
<div id="container">
  <div id="main" role="main">
    <section id="intro">
      <h1>KyoMini PHP</h1>
      <p>欢迎你：<?php echo $_SESSION['username']; ?> | <a href="index.php">返回</a> | <a href="Article_list.php">列表</a> |<a href="Login_out.php"> 安全退出</a></p>
    </section>
    <hr>
    <section id="list">
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tbody>
          <tr>
            <td width="8%" align="center" id="listno">序号</td>
            <td width="59%" id="listno">标题</td>
            <td width="33%" align="center" id="listno">编辑</td>
          </tr>
          <?php foreach($info as $data){ ?>
          <tr>
          <?php  
		  echo "<td align='center'>" . $data["id"] . "</td><td>".$data["title"]."</td><td align='center'><a href='Article_edit.php?id=".$data["id"]. "'>更新</a> | <a href='Article_delete.php?id=".$data["id"]. "'>删除</a></td>";
		  ?>
          </tr>
         <?php } ?>
        </tbody>
      </table>
      <div class="page"> 
        <?php if ($page != 1) { ?>
       <a href="Article_list.php?page=<?php echo $page - 1;?>">上一页</a> 
        <?php } for ($i=1;$i<=$pagenum;$i++) {  //循环显示出页面  ?>
        <?php 
		  if($i==$page){ 
			echo '<b>'.$i.'</b>';
			} else {
			 echo "<a href='Article_list.php?page=".$i."'>".$i."</a>";
		  } 
         ?>
        <?php
          }
           if ($page<$pagenum) { //如果page小于总页数,显示下一页链接
         ?>
        <a href="Article_list.php?page=<?php echo $page + 1;?>">下一页</a>
        <?php } ?>
      </div>
    </section>
  </div>
</div>
</body>
</html>