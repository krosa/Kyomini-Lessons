<?php
    define('KYOMINI_GO',true);	
    require_once('admin_conn.php');
	require_once ('../Config/conn.php"');
    session_start();
	
	$page=isset($_GET['page'])?intval($_GET['page']):1;       
    $num=5;      //设定每页为五页  
    $total=$db->count("article"); 
    $pagenum=ceil($total/$num);    

	if($page>$pagenum || $page == 0){
	   echo "很抱歉，此页没有数据";
	   echo '<script>setTimeout(\'location="Article_list.php"\', 3000);</script>';
	   exit;
	}

	$offset=($page-1)*$num;       
	$info=$db->select("article", array(
		"title",
		"id",),
		 array(
		 "ORDER" => "id DESC",
		 "LIMIT" => array($offset,$num)
	));
?>
<!DOCTYPE html>
<!--[if lt IE 9]><script>window.location.href="../error/index.html"</script><![endif]-->
<html lang="zh-cn">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<title>Admin</title>
<link href="<?php echo $CSS ?>index.css" rel="stylesheet" type="text/css">
</head>
<body>
<div id="container">
  <div id="main" role="main">
    <section id="intro">
      <h1>KyoMini PHP</h1>
      <p>欢迎你：<?php echo $_SESSION['username']; ?> | <a href="index.php">返回</a> | <a href="Article_list.php">列表</a> |<a href="Login_out.php"> 安全退出</a></p>
    </section>
    <hr>
    <section id="list">
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tbody>
          <tr>
            <td width="8%" align="center" id="listno">序号</td>
            <td width="59%" id="listno">标题</td>
            <td width="33%" align="center" id="listno">编辑</td>
          </tr>
          <?php foreach($info as $data){ ?>
          <tr>
          <?php  
		  echo "<td align='center'>" . $data["id"] . "</td><td>".$data["title"]."</td><td align='center'><a href='Article_edit.php?=".$data["id"]. "'>更新</a> | <a href='Article_delete.php?=".$data["id"]. "'>删除</a></td>";
		  ?>
          </tr>
         <?php } ?>
        </tbody>
      </table>
      <div class="page"> 
        <?php if ($page != 1) { ?>
       <a href="Article_list.php?page=<?php echo $page - 1;?>">上一页</a> 
        <?php } for ($i=1;$i<=$pagenum;$i++) {  //循环显示出页面  ?>
        <?php 
		  if($i==$page){ 
			echo '<b>'.$i.'</b>';
			} else {
			 echo "<a href='Article_list.php?page=".$i."'>".$i."</a>";
		  } 
         ?>
        <?php
          }
           if ($page<$pagenum) { //如果page小于总页数,显示下一页链接
         ?>
        <a href="Article_list.php?page=<?php echo $page + 1;?>">下一页</a>
        <?php } ?>
      </div>
    </section>
  </div>
</div>
</body>
</html>