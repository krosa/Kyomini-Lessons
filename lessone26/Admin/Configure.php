<?php require_once('Connections/adminOS2015.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "Login_error.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "up")) {
	mysql_query("SET NAMES utf8"); 
  $updateSQL = sprintf("UPDATE admin_config SET webname=%s, keywords=%s, `description`=%s, mail=%s, `add`=%s, icp=%s WHERE id=%s",
                       GetSQLValueString($_POST['webname'], "text"),
                       GetSQLValueString($_POST['keywords'], "text"),
                       GetSQLValueString($_POST['description'], "text"),
                       GetSQLValueString($_POST['mail'], "text"),
                       GetSQLValueString($_POST['add'], "text"),
                       GetSQLValueString($_POST['icp'], "text"),
                       GetSQLValueString($_POST['id'], "int"));

  mysql_select_db($database_adminOS2015, $adminOS2015);
  $Result1 = mysql_query($updateSQL, $adminOS2015) or die(mysql_error());
}

mysql_select_db($database_adminOS2015, $adminOS2015);
$query_web = "SELECT * FROM admin_config";
mysql_query("set names 'utf8'");
$web = mysql_query($query_web, $adminOS2015) or die(mysql_error());
$row_web = mysql_fetch_assoc($web);
$totalRows_web = mysql_num_rows($web);
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>OFFICE OS</title>
<link href="css/normalize.css" rel="stylesheet" type="text/css">
<script src="js/jquery-1.11.2.min.js" ></script>
<link href="Css/Admin.css" rel="stylesheet" type="text/css"></head>

<body>
   <div class="L-box">
     <h1><a href="Index.php"><img src="img/img03.png"/></a></h1>
        
        <ul>
          <span>基本设置</span>
            <li id="open"><img src="img/icon01.jpg" width="20" height="20" alt=""/>站点设置</li>
          <span>管理数据</span>
            <li>
              <img src="img/icon01.jpg" width="20" height="20" alt=""/>管理数据<br>
              <img src="img/icon01.jpg" width="20" height="20" alt=""/>增加数据
            </li>
        </ul>
</div>
   <div class="R-box">
        <h1>基本设置</h1>
        <form action="<?php echo $editFormAction; ?>" name="up" method="POST" id="up">
        <p><span>网站名称：</span> <input type="text" value="<?php echo $row_web['webname']; ?>" name="webname"></p>
        <p><span>关键词：</span> <input type="text" value="<?php echo $row_web['keywords']; ?>" name="keywords"></p>
        <p><span>描述： </span><input type="text" value="<?php echo $row_web['description']; ?>" name="description"></p>
        <p><span>站长邮箱： </span><input type="text" value="<?php echo $row_web['mail']; ?>" name="mail"></p>
        <p><span>网站地址： </span><input type="text" value="<?php echo $row_web['add']; ?>" name="add"></p>
        <p><span>ICP： </span><input type="text" value="<?php echo $row_web['icp']; ?>" name="icp"></p>
 <input type="hidden" name="id" value="<?php echo $row_web['id']; ?>">
        <input type="submit" value="更新" id="submit">
        <input type="hidden" name="MM_update" value="up">
        </form>
   </div>
   <script>
 $(".L-box span").click(
		function(){
			if($(this).next("li").is(":hidden")) 
				{	
					$(".L-box span li ").slideUp(300);
					$(this).next("li").slideDown(300);
				}
			else
				{
					$(this).next("li").slideUp(300);
				};
		 });
   </script>
</body>
</html>
<?php
mysql_free_result($web);
?>
