<?php require_once('Connections/adminOS2015.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "article")) {
  $insertSQL = sprintf("INSERT INTO admin_article (text, content) VALUES (%s, %s)",
                       GetSQLValueString($_POST['text'], "text"),
                       GetSQLValueString($_POST['content'], "text"));

  mysql_select_db($database_adminOS2015, $adminOS2015);
  mysql_query("set names 'utf8'");
  $Result1 = mysql_query($insertSQL, $adminOS2015) or die(mysql_error());

  $insertGoTo = "Article_success.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $insertGoTo));
}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>OFFICE OS</title>
<link href="css/normalize.css" rel="stylesheet" type="text/css">
<link href="Css/Admin.css" rel="stylesheet" type="text/css">

<script src="js/jquery-1.11.2.min.js" ></script>
</head>

<body>
   <?php include("Nav.php"); ?>
   <div class="R-box">
        <h1>新闻列表</h1>
        <div class="news">
        <form action="<?php echo $editFormAction; ?>" name="article" method="POST" id="article">
         <p>
          文章标题:
           <input type="text" name="text" id="text">
          <br>
             <br>
             文章内容:
              <textarea name="content" cols="50" rows="10"></textarea>  
              <br>
              <br>

           <input type="submit" name="submit" id="submit" value="提交">
         </p>
         <input type="hidden" name="MM_insert" value="article">
         </form>
      </div>
</div>
   <script>
 $(".L-box span").click(
		function(){
			if($(this).next("li").is(":hidden")) 
				{	
					$(".L-box span li ").slideUp(300);
					$(this).next("li").slideDown(300);
				}
			else
				{
					$(this).next("li").slideUp(300);
				};
		 });
   </script>
</body>
</html>
