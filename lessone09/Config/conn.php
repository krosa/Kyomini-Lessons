<?php
header("Content-Type: text/html;charset=utf-8"); 
if (!defined('KYOMINI_GO')) {
	exit('禁止访问');
}
if(PHP_VERSION<'4.1.0'){
    exit('您的版本太低了!');
}
/*
 * 这里是给自己定义常量
 */
$CSS = "Template/CSS/";
$JS = "Template/JS/";
$IMG = "Template/IMG/";
$NEXT="../";
/*
*如果是根目录 请去掉$demo
*/
$demo ="demo";
$HOST = "http://".$_SERVER['HTTP_HOST']."/".$demo."/";
/*
*导航配置
*/
$ABOUT="about.php";
$CONTACT="contact.php";
$HOME="index.php";
$PAGEHOME=substr($_SERVER['REQUEST_URI'],6,10);
$PAGEHOST=substr($_SERVER['REQUEST_URI'],11,11);

?>