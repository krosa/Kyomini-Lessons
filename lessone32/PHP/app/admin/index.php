<?php
include('../conn/route.php');
?>
<!DOCTYPE html>
<html lang="zh-CN">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="renderer" content="webkit">
    <title>Kyomini Admin</title>
    <link href="<? echo ADMIN_CSS ?>bootstrap.min.css" rel="stylesheet">
    <link href="<? echo ADMIN_CSS ?>kyomini.css" rel="stylesheet">
    <!-- respond.min.js 为 IE8 支持HTML5元素 -->
    <!--[if lt IE 9]>
      <script src="<? echo ADMIN_JS ?>html5shiv.min.js"></script>
      <script src="<? echo ADMIN_JS ?>respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

  <div class="navbar navbar-default">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Kyomini Admin</a>
        </div>
        <div class="navbar-collapse collapse navbar-responsive-collapse">
          <ul class="nav navbar-nav">
            <li class=""><a href="#">后台首页</a></li>
            <li><a href="#">快速发布</a></li>
          </ul>
           <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">欢迎你：admin@admin.com <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="#">登入后台</a></li>
                <li><a href="#">资助捐款</a></li>
                <li><a href="#">帮助手册</a></li>
                <li class="divider"></li>
                <li><a href="#">KYOMINI</a></li>
              </ul>
            </li>
          </ul>
  </div>
</div>
  

  
  <div class="container-fluid">
  <div class="sidebar">
                  <div class="sidebar_nav">
          <ul class="unstyled">
            <li><span><a href="#"><i class="icon-wrench"></i>网站设置</a></span>
              <ul class="unstyled" id="hide">
                <li><a href="basic_Set.html"><i class="caret_left" aria-hidden="true"></i>基本配置</a></li>
              </ul>
            </li>
            <li><span><a href="#"><i class="icon-list"></i>分类管理</a></span></li>
           
            <li><span><a href="#"><i class="icon-folder-open"></i>内容管理</a></span>
              <ul class="unstyled" id="hide">
                <li><a href="#"><i class="caret_left"></i>增加内容</a></li>
                <li><a href="#"><i class="caret_left"></i>管理内容</a></li>
              </ul>
            </li>
            <li><span><a href="#"><i class="icon-user"></i>用户管理</a></span>
              <ul class="unstyled" id="hide">
                <li><a href="#"><i class="caret_left"></i>前台用户</a></li>
                <li><a href="#"><i class="caret_left"></i>后台用户</a></li>
              </ul>
            </li>
 
            <li><span><a href="#"><i class="icon-share"></i>其它管理</a></span>
              <ul class="unstyled" id="hide">
                	<li><a href="expand_installation.html"><i class="caret_left"></i>服务器信息</a></li>
                	<li><a href="expand_manage.html"><i class="caret_left"></i>扩展管理</a></li>
              	</ul>
            </li>
          </ul>
        </div>
   </div>
  <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
              <div class="page-header">
          <h3 class="fl">信息概览</h3>
          <div class="user_message fr"><i class="icon-user"></i>当前权限组：管理员<i class="icon-time"></i>最后登录时间 2012-05-30</div>
          <div class="cl"></div>
        </div>
        <div class="jumbotron">
          <h1>欢迎使用 Kyomini PHP ！</h1>
          <p>史上最简单的PHP面向过程开源程序，适合小型网站、博客以及PHP入门初学者使用。以及XHTML基于Bootstrap3.0开发。而且，它是免费的。</p>
          <p>Kyomini还有许多您不知道的小秘密，希望我们在以后的日子里相处愉快。</p>
          <p>&nbsp;</p>
          <p> <a class="btn btn-primary btn-large"> 前往官网中心 </a> </p>
        </div>
        <div class="copyright">&copy; kyomini PHP Beta 0.1</div>
    </div>        
  </div>

    <!-- jQuery-->
    <script src="<? echo ADMIN_JS ?>jquery.min.js"></script>
    <!-- Bootstrap 核心 JS 文件-->
    <script src="<? echo ADMIN_JS ?>bootstrap.min.js"></script>
    <!-- Windows8 IE10 Phone8  BUG-->
    <script src="<? echo ADMIN_JS ?>ie10-viewport-bug.js"></script>
        <!-- 下拉JS-->
	<script type="text/javascript">
        $(document).ready(function() {
             $(".sidebar_nav li span").click(
			 
            function(){ 
                if($(this).next("ul").is(":hidden")) 
                    {	
                        $(".sidebar_nav li ul").slideUp(300);
                        $(this).next("ul").slideDown(300);
                    }
                else
                    {
                        $(this).next("ul").slideUp(300);
                    };
            });	
        });
    </script>
  </body>
</html>