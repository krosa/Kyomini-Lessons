<?php
    define('KYOMINI_GO',true);	
    require_once('admin_conn.php');
    session_start();
	$id=1;
	$in=isset($_SESSION['id']) ? $_SESSION['id'] : '';
	if(!$in == $id) {
			echo "你还没有登陆！";
            echo '<script>setTimeout(\'location="login.php"\', 3000);</script>';
			exit;
	}
	
	
	
?>
<!DOCTYPE html>
<!--[if lt IE 9]><script>window.location.href="../error/index.html"</script><![endif]-->
<html lang="zh-cn">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>Admin</title>
    <link href="<?php echo $CSS ?>index.css" rel="stylesheet" type="text/css">
    <script charset="utf-8" src="<?php echo $EDITOR ?>kindeditor.js"></script>
    <script charset="utf-8" src="<?php echo $EDITOR ?>lang/zh_CN.js"></script>
<script>
        KindEditor.ready(function(K) {
                window.editor = K.create('#editor_id',{
					themeType : 'simple'
					});
        });
</script>
</head>

    <body>
<div id="container">
        <div id="main" role="main">
            <section id="intro">
              <h1>KyoMini PHP</h1>
              <p>欢迎你：<?php echo $_SESSION['username']; ?> | 列表 |<a href="Login_out.php"> 安全退出</a></p>
            </section>
            <hr>
            <section id="same-network">
               <form method="POST" action="Article_manage.php">
                  <label for="textfield">文章标题:</label>
                  <input type="text" name="title" class="article a_input">
                  <label for="textfield">文章内容:</label>
                  <textarea rows="10" name="content" id="editor_id" class="article a_con"></textarea><br>

                  <input type="submit" name="submit" id="submit" value="发布" class="a_submit">
                </form>
            </section>
      </div>
</div>
</body>
</html>